<html>

<head>
  <title>Art Works</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
</head>

<body>
  <div class="header"></div>
  <div class="page">
    <!-- <div class="sidebar">
      <div class="nav">Add</div>
    </div> -->
    <div class="main">
      <div class="row art-pagetitle">
        <div class="col-sm-11 title"></div>

        <a href="edit.php">
          <div class="btn btn-secondary">Edit</div>
        </a>
      </div>
      <div class="art-detail">
        <div class="row">

          <div class="col-sm-4 title">Name of Artwork</div>
          <div class="col-sm-8">The Man who saw tomorrow</div>
        </div>
        <div class="row">
          <div class="col-sm-4 title">Art Type</div>
          <div class="col-sm-8">Oil Painting</div>
        </div>
        <div class="row">
          <div class="col-sm-4 title">Price</div>
          <div class="col-sm-8">#50,000</div>
        </div>
        <div class="row">
          <div class="col-sm-4 title">Description</div>
          <div class="col-sm-8"></div>
        </div>
        <div class="row">
          <div class="col-sm-4 title">Image</div>
          <div class="col-sm-8"></div>
        </div>
        <div class="row">
          <div class="col-sm-4 title">QR</div>
          <div class="col-sm-8"></div>
        </div>
      </div>
    </div>
  </div>
</body>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
  integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</html>