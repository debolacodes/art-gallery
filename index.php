<html>

<head>
  <title>Art Works</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
</head>

<body>
  <div class="header"></div>
  <div class="page">
    <!-- <div class="sidebar">
      <div class="nav">Add</div>
    </div> -->
    <div class="main">

      <div class="row art-pagetitle">
        <div class="col-sm-11 title">
          <p>Here is a List of all your Artworks.</p>
        </div>
        <div class="btn btn-secondary" data-toggle="modal" data-target="#add_art">Add</div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>

      <div class="grid row">
        <div class="image col-sm-3" style="background:url('/image/art.jpeg'); background-size:cover"></div>
        <div class="text col-sm-3 pt-sm-5">
          <h3>The Man who saw tomorrow</h3>
          <p>By: Adewale Kuku</p>
        </div>
        <div class="actions col-sm-3 pt-sm-5">
          <p><a href="details.php">View</a> |<a href="edit.php"> Edit </a> |<span data-toggle="modal"
              data-target="#delete_art"> Delete </span>
          </p>
        </div>
        <div class="main-action col-sm-3 pt-sm-5">
          <div class="btn btn-primary">Print QR Code </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="add_art" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Art</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form>
            <div class="form-group">
              <label for="name">Name of Artwork</label>
              <input type="text" class="form-control" id="name" placeholder="Name of Artwork">
            </div>
            <div class="form-group">
              <label for="type">Type</label>
              <input type="text" class="form-control" id="type" placeholder="Art Type">
            </div>
            <div class="form-group">
              <label for="price">Price</label>
              <input type="num" class="form-control" id="price" placeholder="Name of Artwork">
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" id="description" rows="3"></textarea>
            </div>

            <div class="form-group">
              <input type="file" name="file" class="" />
            </div>

            <div class="form-group">
              <input name="submit" type="submit" class="form-control btn btn-primary">
            </div>

          </form>




        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="delete_art" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Art</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <p>Are you sure you want to delete this artwork.</p>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger">Delete</button>
          </div>

        </div>
      </div>
    </div>
  </div>

</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
  integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</html>